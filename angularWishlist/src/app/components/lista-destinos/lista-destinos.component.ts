import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { destinoViaje } from './../models/destino-viaje.model';
import { destinosApiClient } from './../models/destinos-api-client.model';
import { ElegidoFavoritoAction, NuevoDestinoAction} from '../models/destinos-viajes-state.model';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
 @Output() onItemAdded: EventEmitter<destinoViaje>;
 updates: string;
 constructor(private destinosApiClient: DestinosApiClient, private store: store<AppState>) { 
   this.onItemAdded = new EventEmitter();
   this.updates = [];
   this.store.select(state => state.destinos.favorito)
   .subscribe(d => {
    if (d != null) {
      this.updates.push('se ha elegido a ' + d.nombre);
    }
   });
 }

 ngOnInit(): void {
 }
 agregado(d: destinoViaje){
   this.destinosApiClient.add(d);
   this.onItemAdded.emit(d);
   this.store.dispatch(new NuevoDestinoAction(d));
 }

 elegido(e: destinoViaje){
  this.destinosApiClient.elegir(e);
  this.store.dispatch(new ElegidoFavoritoAction(e));
}
}
