import { destinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';

export class destinosApiClient{
	constructor(private store: Store<AppState>){
		
	}
	add(d: destinoViaje){
		this.store.dispatch(new NuevoDestinoAction(d));
	}
	elegir(d: destinoViaje) {
		this.store.dispatch(new ElegidoFavoritoAction(d));	
	}
		
}

