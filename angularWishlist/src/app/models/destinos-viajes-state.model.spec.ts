import {
	reducerDestinosViajes,
	DestinosViajesState,
	initializeDestinosViajesState,
	InitMyDataAction,
	NuevoDestinoAction
} from './destinos-viajes-state.model';
import { destinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
	it('should reduce init data', () => {
		const prevState: DestinosViajesState = initializeDestinosViajesState();
		const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		expect(newState.items.lenght).toEqual(2);
		expect(newState.items[0].nombre).toEqual('destino 1');
	});

	it('should reduce new item added', () => {
		const prevState: DestinosViajesState = initializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new destinoViaje('barcelona', 'url'));
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		expect(newState.items.lenght).toEqual(1);
		expect(newState.items[0].nombre).toEqual('barcelona')
	});
});