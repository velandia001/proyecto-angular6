import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {  NgxMapboxGLModule } from 'ngx-mapbox-gl'
import {  BrowserAnimationsModule } from '@angular/platform-browser/animations'


import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { DestinosViajesState, ReducerDestinosViajes, InitializeDestinosViajesState } from './models/destinos-viajes-state.model';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasApiClientComponent } from './reservas/reservas/reservas-api-client/reservas-api-client.component';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';



export const childrenRoutesVuelos: Routes = [
   {path: '', redirectTo: 'main', pathMatch: 'full'},
   {path: 'main', component: VuelosMainComponent},
   {path: 'mas-info', component: VuelosMasInfoComponent},
   {path: ':id', component: VuelosDetalleComponent},
];


const routes: Routes = [
{ path: '', redirectTo: 'home', pathMatch: 'full'},
{ path: 'home', component: ListaDestinosComponent},
{ path: 'destino/:id', component: DestinoDetalleComponent},
{ path: 'login', component: LoginComponent},
{
  path: 'protected',
  component: ProtectedComponent,
  canActivate: [ UsuarioLogueadoGuard ]
},
{
  path: 'vuelos',
  component: VuelosComponent,
  canActivate: [UsuarioLogueadoGuard],
  children: childrenRoutesVuelos
}
];

//redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: ReducerDestinosViajes;
};

let reducersInitialState = {
  destinos: InitializeDestinosViajesState();
};



//redux fin init



@NgModule({
  declarations: [
    AppComponent,
    ListaDestinosComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    ReservasApiClientComponent,
    EspiameDirective,
    TrackearClickDirective
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajeEffects]),
    StoreDevtoolsModule.instrument(),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
  DestinosApiClient], AuthService, UsuarioLogueadoGuard

  bootstrap: [AppComponent]
})
export class AppModule { }
